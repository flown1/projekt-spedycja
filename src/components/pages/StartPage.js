import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from "react-router";
export default class StartPage extends Component{
  render(){
    return(
      <StyledContainer>
        <StyledText>Tutaj poczujesz się jak w domu!</StyledText>
        <StyledLink to="/login">Zaloguj się!</StyledLink>
      </StyledContainer>
    )
  }
}

const StyledLink = styled(Link)`
font-size: 2vw;
text-decoration: none;
color: orange;
&:hover{
  text-decoration: none !important;
}
`;

const StyledContainer = styled.div`
position: absolute;
width:  40%;
height: 40%;
border-radius: 8px;
box-shadow: 1px 1px 1px #666;
padding-bottom: 3%;
margin: 0 auto;
display: flex;
flex-direction: column;
flex-wrap: wrap;
justify-content:center;
border:solid 3px;
border-color: white;
background-color: #0294A5;
align-items:center;
`;

const StyledText = styled.p`
font-size: 2.5vw;
color:white;
margin: 1vw 0 2vw 0;
`