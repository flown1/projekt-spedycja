import React, { Component } from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import Button from 'react-bootstrap/lib/Button';
import * as LoginAction from '../../store/LoginActions';
import axios from 'axios';

class Setting extends Component{
    constructor(props){
        super();
        this.state = {
          email: '',
          password: '',
          number: ''
        };
        this.onInputEmailChange = this.onInputEmailChange.bind(this);
        this.onInputPasswordChange = this.onInputPasswordChange.bind(this);
        this.onInputNumberChange = this.onInputNumberChange.bind(this);
      }
 
 onSubmit(){
    const ROOT_URL = "https://spedition-app.herokuapp.com";
    let self = this;
    axios.post(ROOT_URL + '/auth/login',{
      phone_number: self.state.number
    })
      .then(function (response) {
            console.log("Wysłano update!")
      })
      .catch(function (error) {
        alert("Invalid login or password! Try again!");
      });
  }

  onInputEmailChange(e){
    this.setState({email: e.target.value});
  }
  onInputPasswordChange(e){
    this.setState({password: e.target.value});
  }

  onInputNumberChange(e){ 
   this.setState({number: e.target.value});
  }

  onSubmitEmailData(e){
    e.preventDefault()    
    this.props.onChangeEmail(this.state.email);
  }

  onSubmitPasswordData(e){
    e.preventDefault();   
    const pass = this.state.password; 
    console.log(pass);
    if(pass !== ""){
      this.props.onChangePassword(pass);
      let updatePackage = {"password": pass};
      this.updater(updatePackage);
    }
  }
  updater(updatePackage){
    
    // axios.put('http://spedition-app.herokuapp.com/user/', updateObj)
    //                 .then((response)    => {
    //                         console.log(response, self.props.assignment.status_id);
                            
    //                         self.fetchAssigments(this.props.user.token);
    //                     },
    //                     (err)         => {alert(err + 'check console logs'); console.log(err)}
    //                 );
  }
  onSubmitUserData(e){    
      e.preventDefault()    
      this.props.onChangeNumber(this.state.number);
      this.onSubmit();
  }


  render(){
      console.log(this.props.user)
    return(
      <StyledContainer>
          <h1 style={{color:"white"}}>Ustawienia</h1>
          <StyledBoxForm>
                         <StyledForm className="form" role="form" id="login-nav">
                            <div style={{display:"flex", justifyContent:"center",flexDirection:"column"}} className="form-group">
                                <p>Aktualny email: {this.props.user.email}</p>
                                <input className="form-control" id="exampleInputEmail2" onChange={this.onInputEmailChange} style={{textAlign: "center",width: "90%",alignSelf:"center"}} placeholder="Podaj nowy email" required/>
                            </div>
                            <StyledButton type="submit"  className="btn btn-success" onClick={this.onSubmitEmailData.bind(this)} >Zmień!</StyledButton>                              

                            <div style={{display:"flex", justifyContent:"center",flexDirection:"column", marginTop: '10px'}} className="form-group">
                                <p>Podaj nowe hasło:</p>
                                <input type="form-control" className="form-control" id="exampleInputPassword2" onChange={this.onChangeInputPassword} style={{textAlign: "center",width: "90%",alignSelf:"center"}} placeholder="Podaj nowe hasło" required/>
                            </div>
                                <StyledButton type="submit"  className="btn btn-success" onClick={this.onSubmitPasswordData.bind(this)} >Zmień!</StyledButton>                              
                        </StyledForm>
                        <StyledForm2 className="form" role="form" id="login-nav">
                            <div style={{display:"flex", justifyContent:"center",flexDirection:"column"}}className="form-group">
                                <p>Aktualny number: {this.props.user.number}</p>
                                <input type="form-control" className="form-control"  onChange={this.onInputNumberChange} style={{textAlign: "center",width: "90%",alignSelf:"center"}} placeholder="Podaj nową firmę" required/>
                            </div>
                                <StyledButton type="submit"  className="btn btn-success" onClick={this.onSubmitUserData.bind(this)} >Zmień!</StyledButton>                              
                        </StyledForm2>
                       </StyledBoxForm>
      </StyledContainer>
    )
  }
}

const mapStateToProps = (state) => {
    return { user: state.user};  };

const mapDispatchToProps = (dispatch) => {
    return {onChangeName: (value) => dispatch(LoginAction.setName(value)),
    onChangeSurname: (value) => dispatch(LoginAction.setSurname(value)),
    onChangeCompany: (value) => dispatch(LoginAction.setCompany(value)),
    onChangeEmail: (value) => dispatch(LoginAction.setEmail(value)),
    onChangePassword: (value) => dispatch(LoginAction.setPassword(value)),
    onChangeNumber: (value) => dispatch(LoginAction.setNumber(value))
}};

export default connect(mapStateToProps,mapDispatchToProps)(Setting);

const StyledContainer = styled.div`
border-radius: 8px;
border: 3px solid;
display: flex;
flex-direction: column;
justify-content:space-around;
align-items: center;
color: black;
background-color:#0294A5;
width: 650px;
height: 400px;
padding-bottom: 30px;`

const StyledBoxForm = styled.div`
margin-top: -3vw;
border-radius: 8px;
display: flex;
flex-direction: row;
justify-content:space-around;
align-items: center;
color: black;
background-color: none;
width: 640px;
height: 400px;`

const StyledForm = styled.div`
margin-top: 50px;
display:flex;
justify-content:center;
flex-direction:column;
text-align: center;
font-size:1.3vw;`


const StyledForm2 = styled.div`
margin-top: -120px;
display:flex;
flex-direction:column;
text-align: center;
font-size:1.3vw;`

const StyledButton = styled(Button)`
align-self:center;
width: 40%;
`

