import React, { Component } from 'react';
import styled from 'styled-components';
import profileImg from '../resources/driverPhotoDummy.jpg';
import AssignmentDetails from '../resources/AssignmentDetails';
import {connect} from 'react-redux';
import Map from '../resources/Map';
import FinishAssignmentButton from '../resources/FinishAssignmentButton';

class DriverOverview extends Component{
  constructor(props){
    super(props);
    this.state = {
      photo: profileImg,
      mapCenter:   {lat: 45.45, lng: 54.54},
      neverFetched: true,
      name: this.props.user.name || '---',
      surname: this.props.user.surname || '---',
      number: this.props.user.number || '---'
    }
  }

  render(){
    return(
      <StyledContainer>
        <DriverDetails>
        <ImgStyled src={this.state.photo} alt="profile picture"/>
        <DriverNameContainer>
            <p style={{justifyContent: "center", display: "flex", color: "black"}}>Kierowca: {this.state.name} {this.state.surname}</p>
            <p style={{justifyContent: "center", display: "flex",color: "black"}}>Numer telefonu: {this.state.number}</p>
            </DriverNameContainer>
        </DriverDetails>
        <AssignmentDetails />
        <FinishAssignmentButtonStyle>
          <FinishAssignmentButton /> 
        </FinishAssignmentButtonStyle>
        <Map
          containerElement={<div style={{height: '20vw', width:  '92%', margin: "0 auto 4%"}} />}
          mapElement={<div style={{height: '20vw', width:  '92%', margin: "0 auto 4%", border: "3px solid", borderRadius: "10px"}} />} />
      </StyledContainer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  user: state.user,
  showLogin: state.showLogin,
  };
};

export default connect(mapStateToProps)(DriverOverview);

const ImgStyled = styled.img`
color:black;
border: 2px solid;
border-radius: 50% !important;
width: 10vw !important;
height: 10vw !important;
margin-right: 2% !important;`

const DriverNameContainer = styled.div`
display: flex;
flex-direction: column;
text-align: left;
margin-left: 2%;
`
const FinishAssignmentButtonStyle = styled.div`
margin: 15px auto;`

const StyledContainer = styled.div`
width:700px;
margin-top: 250px;
background-color: #0294A5;
border-radius: 8px;
box-shadow: 1px 1px 1px #666;
display: flex;
flex-direction: column;
justify-content:center;
border:solid 3px;
border-color: white;
font-family: 'Racing Sans One', cursive;
`;

const DriverDetails = styled.div`
font-size: 1.8em;
margin: 2% 0 2% 0;
display: flex;
align-items:center;
justify-content: center;
flex-direction: row;
width: 100%;
`;
