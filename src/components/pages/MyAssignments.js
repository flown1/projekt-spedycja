import React, {Component} from 'react';
import styled from 'styled-components';
import AssigmentsList from '../resources/AssigmentsList';
import {connect} from 'react-redux';

class MyAssignments extends Component{
  constructor(props) {
    super(props);
    this.state = {searchQuery:  ""};
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  
  handleChange = e =>{
    let buff = this.capitalizeFirstLetter(e.target.value)
    this.setState({searchQuery: buff});
  }

    render(){
      const returnSearch = this.props.myorders
      .filter(p => p.start_location.city.includes(this.state.searchQuery))
      .reverse();
      return(
        <StyledContainer>
                <h2>Zlecenia:</h2>
                  <StyledSearchBar>
                    <div className="form-group">
                      <input type="text" onChange={this.handleChange} className="inputSearchAssigments form-control" placeholder="Wyszukaj zlecenie (Miasto początkowe)"/>
                    </div>
                  </StyledSearchBar>
                <StyledAssigmentListBox><AssigmentsList orders={returnSearch} type="my" /></StyledAssigmentListBox>
        </StyledContainer>
      );
    }
  }

const mapStateToProps = (state) => {
  return {
    myorders: state.myorders
  };
};


export default connect(mapStateToProps)(MyAssignments);


  const StyledAssigmentListBox = styled.div`
  align-self:center;
  ` 

  const StyledContainer = styled.div`
  overflow: scroll;
  margin-top: 350px;
  background-color: #0294A5;
  border-radius: 8px;
  box-shadow: 1px 1px 1px #666;
  border:solid 3px;
  border-color: white;
  display:flex;
  flex-direction:column;
  align-items;center;
  text-align:center;
  width: 800px;
  height: 900px;
  padding-bottom: 1.5%;
  `;

  const StyledSearchBar= styled.div`
  .inputSearchAssigments{
      width: 60%;
      margin: 0 auto;
  }
`