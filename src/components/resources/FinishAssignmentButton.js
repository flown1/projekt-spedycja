import React, { Component } from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import axios from 'axios';
import * as LoginAction from '../../store/LoginActions';
import { hashHistory } from "react-router";

class FinishAssignmentButton extends Component {
    constructor(props){
        super();
        this.state = {"buttonActive": true};
        this.handleOnClick = this.handleOnClick.bind(this);
    }
    fetchAssigments(token){
        const ROOT_URL = 'https://spedition-app.herokuapp.com';
        let self = this;
        axios.get(ROOT_URL + '/orders/my',{
          headers:{
            'Authorization':  token
          }
        })
          .then(function (response) {
            console.log("wchodzi zlecenie");
            let data = response.data.filter((item) => {
              return item.status.id === 3;
            });
            console.log(data);
            self.props.onChangeAssignmentID(data[0].id);
            self.props.onChangeStartLocationName(data[0].start_location.city);
            self.props.onChangeFinishLocationName(data[0].finish_location.city);
            self.props.onChangeStartDate(data[0].start_date);
            self.props.onChangeFinishDate(data[0].finish_date);
            self.props.onChangeStatusID(data[0].status.id);
            self.props.onChangeVehicleID(data[0].vehicle_id);
            self.props.onChangeWareName(data[0].ware.type_name);
            self.props.onChangeLocation(
              data[0].start_location.longitude,
              data[0].start_location.latitude,
              data[0].finish_location.longitude,
              data[0].finish_location.latitude);
            self.setState({val: true})
        })
        .catch(function (error) {
          console.log("Used token: ", token);
          console.log("Nie mozna pobrac zlecenia!");
        });
    
    }
    fetchData(){
        const ROOT_URL = "http://spedition-app.herokuapp.com";
        let self = this;
        axios.get(ROOT_URL + '/orders')
          .then(function (response) {
            console.log("wchodza zlecenia");
            console.log(response);
            self.props.onChangeOrders(response.data);
            self.props.onChangeLoader();
          })
          .catch(function (error) {
            alert("Błąd. Nie udało się wyświetlić zleceń")
            console.log(error);
          });
    }
    btnActiveAfter(time){
        setTimeout(()=>{
            this.setState({'buttonActive': true});
        }, time);
    }
    handleOnClick(e){
        hashHistory.push('/loader')
        this.btnActiveAfter(2000);
        if(this.props.assignment !== undefined){
            this.setState({'buttonActive': false});
            if(this.props.assignment.status_id !== 5){
                let id = this.props.assignment.assignment_id;
                const deliveredStatusId = 5,
                    deliveredStatusName = "Delivered";
                console.log("Will update close order");
                const newStatus = {"status_id": deliveredStatusId, "name": deliveredStatusName};
                let self = this;
                axios.put('http://spedition-app.herokuapp.com/orders/'+id, newStatus)
                    .then((response)    => {
                            console.log(response, self.props.assignment.status_id);
                            console.log("Closed Order Will Try To Update STORE");
                            self.fetchAssigments(this.props.user.token);
                        },
                        (err)         => {alert(err + 'check console logs'); console.log(err)}
                    );
            }else{
                alert("Zlecenie jest już DOSTARCZONE");
            }
        }else{
            console.log("Assignment is undefined");
        }
    }
    render(){
        return (
            <StyledComponent>
                <button className={this.state.buttonActive ? "btn btn-success"  : "btn btn-success disabled"} onClick={this.handleOnClick}>Wykonane</button>
            </StyledComponent> 
        );
    };
};
const mapStateToProps = (state) => {
    return {
        assignment: state.assignment,
        user:       state.user
    };
};
  
const mapDispatchToProps = (dispatch) => {
    return {
        onChangeAssignmentID: (value) => dispatch(LoginAction.setAssignmentId(value)),
        onChangeStartLocationName: (value) => dispatch(LoginAction.setStartLocation(value)),
        onChangeFinishLocationName: (value) => dispatch(LoginAction.setFinishLocation(value)),
        onChangeStartDate: (value) => dispatch(LoginAction.setStartDate(value)),
        onChangeFinishDate: (value) => dispatch(LoginAction.setFinishDate(value)),
        onChangeStatusID: (value) => dispatch(LoginAction.setStatusID(value)),
        onChangeVehicleID: (value) => dispatch(LoginAction.setVehicleID(value)),
        onChangeWareName: (value) => dispatch(LoginAction.setWareName(value)),
        onChangeLocation: (val1,val2,val3,val4) => dispatch(LoginAction.setTude(val1,val2,val3,val4)),
        onChangeOrders: (value) => dispatch(LoginAction.setOrders(value))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(FinishAssignmentButton);
  
const StyledComponent = styled.div`
    width: 100px;
`;