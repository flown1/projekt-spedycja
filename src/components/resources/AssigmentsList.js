import React, { Component } from 'react';
import AssigmentsListItem from './AssigmentsListItem';
import styled from 'styled-components';

class AssigmentsList extends Component{
  render(){
    if(this.props.type==="my"){
    return(
      <StyledList>
        <StyledBox style={{width: "670px", marginLeft: "-10px"}}>
            <div>ID</div>
            <div>Towar</div>
            <div>Start</div>
            <div>Koniec</div>
            <div>Status</div>
      </StyledBox>
          {this.props.orders.map( (item) => {
            return <AssigmentsListItem key={item.id}
                                        id={item.id}
                                        ware={item.ware.type_name}
                                        source={item.start_location.city}
                                        destination={item.finish_location.city}
                                        vehicle={item.vehicle_id}
                                        status = {item.status.name}
                                        type = {this.props.type}/>
          })}

      </StyledList>
    )}
    else{
      return(
        <StyledList>
          <StyledBox>
              <div>ID</div>
              <div>Towar</div>
              <div>Start</div>
              <div>Koniec</div>
              <div>Status</div>
        </StyledBox>
            {this.props.orders.map( (item) => {
              return <AssigmentsListItem key={item.id}
                                          id={item.id}
                                          ware={item.ware.type_name}
                                          source={item.start_location.city}
                                          destination={item.finish_location.city}
                                          vehicle={item.vehicle_id}
                                          status = {item.status.name}
                                          type = {this.props.type}/>
            })}
  
        </StyledList>
      )
    }
  }
}


export default AssigmentsList;

const StyledList = styled.div`
  width: 800px;
  height: 800px;
  display: flex;
  align-items:center;
  flex-direction:column;
`;

const StyledBox = styled.div`
font-size: 1.5em;
display: flex;
flex-direction: row;
justify-content: space-between;
width: 530px;
margin-left: -140px;
margin-bottom:25px;
`
