import React, {Component} from 'react';
import Button from 'react-bootstrap/lib/Button';
import styled from 'styled-components';
import {connect} from 'react-redux';
import * as LoginAction from '../../store/LoginActions';
import { Link } from "react-router";


class LoginForm extends Component{
  constructor(props){
    super();

    this.state = {
      email: '',
      password: '',
      auth_token: ''
    };
    this.onInputEmailChange = this.onInputEmailChange.bind(this);
    this.onInputPasswordChange = this.onInputPasswordChange.bind(this);
  }


  onInputEmailChange(e){
    this.setState({email: e.target.value});
  }
  onInputPasswordChange(e){
    this.setState({password: e.target.value});
  }



onClick(){
  console.log(this.state);
  this.props.onChangeStatus();
  this.props.onChangeEmail(this.state.email);
  this.props.onChangePassword(this.state.password);
}



  render(){
    return(
        <StyledContainer>
          <StyledImgLogo src="https://image.flaticon.com/icons/png/128/259/259551.png"/>
         <StyledTitle>Logowanie</StyledTitle>
                       <StyledForm className="form" role="form" method="post" action="login" id="login-nav">
                          <div className="form-group">
                             <label className="sr-only" htmlFor="exampleInputUsername2">Login</label>
                             <input className="form-control" id="exampleInputEmail2" onChange={this.onInputEmailChange} style={{textAlign: "center"}} placeholder="Login" required/>
                          </div>
                          <div className="form-group">
                             <label className="sr-only" htmlFor="exampleInputPassword2">Haslo</label>
                             <input type="password" className="form-control" id="exampleInputPassword2" onChange={this.onInputPasswordChange} style={{textAlign: "center"}} placeholder="Password" required/>
                          </div>
                             <Button type="submit"  className="btn btn-success" onClick={this.onClick.bind(this)}><Link style={{textDecoration: "none", color:"white", display: "block", width:"100%",height:"100%"}} to="/loader">Zaloguj</Link></Button>
                       </StyledForm>
        </StyledContainer>
    )
  }
}
const mapStateToProps = (state) => {
  return { showLogin: state.showLogin};
};

const mapDispatchToProps = (dispatch) => {
  return {onChangeStatus: () => dispatch(LoginAction.setTrue()),
    onChangeEmail: (value) => dispatch(LoginAction.setEmail(value)),
    onChangePassword: (value) => dispatch(LoginAction.setPassword(value)),
    onChangeUserToken: (value) => dispatch(LoginAction.setUserToken(value))};
};

export default connect(mapStateToProps,mapDispatchToProps)(LoginForm);


const StyledContainer = styled.div`
width: 350px;
height:300px;
background-color: #0294A5;
display:flex;
justify-content:center;
align-items:center;
flex-direction:column;
padding-bottom:20px;
border-style:solid;
border-color: white;
border-radius:15px;
opacity:0.95;
margin-top: 100px;
`;

const StyledForm = styled.div`
display:flex;
justify-content:center;
flex-direction:column;`


const StyledTitle = styled.h1`
color:white;
margin-bottom: 8%;
`;
const StyledImgLogo = styled.img`
height: 4vw;
filter: invert(100%);
margin-bottom:-25px;
`
