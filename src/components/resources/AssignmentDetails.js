import React, { Component } from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import Moment from 'react-moment';
import 'moment/locale/pl'

class AssigmentDetails extends Component {

  constructor(props){
    super();
    this.state = {
      neverFetched: true,
      assigments: [{}],
    }
  }

  render(){
    return(
      <StyledContainerDetails>
      <LeftBox>
            <StyledBox style={{marginBottom: "10px"}}>

                  <div className="box-title">Towar:</div>

                  <div className="box-details">{this.props.assignment.ware_name || "Brak"}</div>

              </ StyledBox>

              <StyledBox>

                  <div className="box-title">Termin:</div>

                  <div className="box-details">  <Moment format="YYYY/MM/DD">
             {this.props.assignment.finish_date}</Moment></div>

                  </StyledBox>
                  </LeftBox>
                  <RightBox>
              <StyledBox style={{marginBottom: "10px"}}>

                  <div className="box-title">Czas do końca:</div>

                  <div className="box-details"><Moment locale="pl" fromNow>{this.props.assignment.finish_date}</Moment></div>

              </StyledBox>


              <StyledBox>
            <div className="box-title">Trasa:</div>
    <div className="box-details">{this.props.assignment.start_location_name || ""} - {this.props.assignment.finish_location_name || ""}</div>
            </StyledBox>
      </RightBox>
      </StyledContainerDetails>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    userToken: state.user.token,
    assignment: state.assignment
  };
};



export default connect(mapStateToProps)(AssigmentDetails);

const StyledContainerDetails = styled.ul`
display: -webkit-box;
display: -moz-box;
display: -ms-flexbox;
display: -moz-flex;
display: -webkit-flex;
display: flex;
-webkit-flex-direction: row;
flex-direction: row;
justify-content: center;
align-items:center;
color:  black;
margin-left: -28px;
width:100%;
height: 20vw;
align-self:center;
`;


const LeftBox = styled.div`
display:flex;
justify-content: center;
align-items:center;
flex-direction:column;
width: 80%;
height: 90%;`

const RightBox = styled.div`
display:flex;
justify-content: center;
align-items:center;
flex-direction:column;
width: 80%;
height: 90%;`

const StyledBox = styled.div`
width:80%;
height:60%;
border-radius: 8px;
background-color: #fdfdfd;
display:flex;
justify-content:center;
align-items:center;
border:solid 2px;
border-color:#3e5269;
flex-direction:column;
font-size: 1.4vw;

  .box-details{
    color: green;
    font-size: 1.3vw;
  }
`;

/*        self.props.onChangeAssignmentID(response.data[0].id);
        self.props.onChangeStartLocationID(response.data[0].start_location_id);
        self.props.onChangeFinishLocationID(response.data[0].finish_location_id);
        self.props.onChangeStartDate(response.data[0].start_date);
        self.props.onChangeFinishDate(response.data[0].finish_date);
        self.props.onChangeStatusID(response.data[0].status_id);
        self.props.onChangeVehicleID(response.data[0].vehicle_id);
        self.props.onChangeWareID(response.data[0].ware_id);*/
