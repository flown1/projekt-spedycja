import React, { Component } from 'react';
import { withGoogleMap,GoogleMap, Marker,  TrafficLayer } from "react-google-maps";
import imgMarkerA from './markerA.png';
import imgMarkerB from './markerB.png';
import {connect} from 'react-redux';

class Map extends Component {
  constructor(props){
    super(props);
    this.state = {
      center:   {lat: 52.22977, lng: 21.01178},
      markers:  [{
        key:      "A",
        position: {
                  lat: this.props.assignment.start_location_latitude,
                  lng: this.props.assignment.start_location_longitude
                  },
        showInfo: false,
        icon:     imgMarkerA

      },{
        key:      "B",
        position: {
                  lat: this.props.assignment.finish_location_latitude,
                  lng: this.props.assignment.finish_location_longitude,
                  },
        showInfo: false,
        icon:     imgMarkerB
      }],
      driverPos: {
        position: {lat: 51, lng: 19},
        showInfo: false
      }
    }
  }
  onMarkerClick(marker){
    console.log("Clicked on marker " + marker.position.lat+ " " +marker.position.lng);
  }
  onDriverMarkerClick(){
    console.log("Clicked on driver");
  }
  componentWillMount(){
    let self = this;
    if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function success(position){
            let coords = {lat: position.coords.latitude, lng: position.coords.longitude}
            self.setState({center: coords})
            self.setState({driverPos:  {position: coords}})
          });
      } else {
          console.log("Geolocation is not supported by this browser.");
      }
  }
  render() {
    const markers = this.state.markers || []
    return (
      <div style={{zIndex: '2'}}>
        <GoogleMap
        style={{zIndex: '2 !important'}}
            defaultCenter={this.props.center}
            defaultZoom={12}
            center={this.state.center}>

              {markers.map((marker, index) => (
                  <Marker {...marker}
                      onClick={() => this.onMarkerClick(marker)}
                  />
              )
              )}

            <Marker position = {this.state.driverPos.position}
                    icon = {this.state.driverPos.icon}
                    showInfo = {this.state.driverPos.showInfo}
                    onClick={() => this.onDriverMarkerClick()}/>
            <TrafficLayer autoUpdate />
        </GoogleMap></div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
  assignment: state.assignment
  };
};

export default connect(mapStateToProps)(withGoogleMap(Map));
