import React, {Component} from 'react';
import styled from 'styled-components';
import * as LoginAction from '../../store/LoginActions';
import axios from 'axios';
import {connect} from 'react-redux';
import { hashHistory } from "react-router";

class AssigmentsListItem extends Component{
  constructor(props){
    super();
    this.state = {
      id: props.id,
      ware: props.ware || "---",
      source: props.source|| "---",
      destination: props.destination|| "---",
      description: props.description || "---",
      vehicle_id: props.vehicle_id || "---",
      status_id: props.status || "---",
      buttonEnabled: true,
      label: "Rezerwuj"
    }
    this.handleReserveClick = this.handleReserveClick.bind(this)
  }

  handleReserveClick(){
    
    let self = this;
    axios.post('http://spedition-app.herokuapp.com/orders/'+this.state.id+"/assign", {},{
      headers:{
        'Authorization':  this.props.user.token
      }
    })
      .then((response)    => {
                console.log("POST successful");
                self.setState({buttonEnabled: false,
                              label:   "Zarezerwowales"});
                hashHistory.push('/loader')
            },
            (err)         => {alert(err + 'check console logs'); console.log(err)}
      );
  }

  render(){
    if(this.props.type==="db"){
    if( this.props.status === "Opened"){
      return(
        <StyledItem>
          <p>{this.state.id}</p>
          <p>{this.state.ware}</p>
          <p>{this.state.source}</p>
          <p>{this.state.destination}</p>
          <p>{this.state.status_id}</p>
            <button className={this.state.buttonEnabled ? "btn btn-success"  : "btn btn-success disabled"}
              onClick={this.handleReserveClick.bind(this)}
            >{this.state.label}
            </button>
  
        </StyledItem>
      )
    }
    else{
      return null;
    }
  }else if(this.props.type==="my"){
    if(this.props.status === "Assigned"){
        return( <StyledItem>
            <p>{this.state.id}</p>
            <p>{this.state.ware}</p>
            <p>{this.state.source}</p>
            <p>{this.state.destination}</p>
            <p>{this.state.status_id}</p>
          </StyledItem>)
      }
      else{
        return null;
      }
    }

  }
}
const mapStateToProps = (state) => {
  return { 
    user: state.user
  };
};

const StyledItem = styled.div`
width: 700px;
background-color: white;
padding: 10px;
margin: 2px 0 2px 0;
border-radius: 6px;
border: 2px solid black;
display:flex;
flex-direction: row;
justify-content:space-between;
:hover{
  background-color: gray;
}`;


export default connect(mapStateToProps)(AssigmentsListItem);