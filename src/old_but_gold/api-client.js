import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://praktyki-react.herokuapp.com",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  }
});
const configureApi = store => {
  apiClient.interceptors.request.use(
    function(config) {
      const state = store.getState();
      if (state.session.token) {
        config.headres["X-User-Email"] = state.session.email;
        config.headres["X-User-Token"] = state.session.token;
      }
      console.log(state);
      return config;
    },
    function(error) {
      // Do something with request error
      return Promise.reject(error);
    }
  );
};

export { configureApi };
export default apiClient;
