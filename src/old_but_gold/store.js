import { createStore, combineReducers } from "redux";

const initialState = {
  postsCollection: [
    { title: "one", timestamp: 1 },
    { title: "two", timestamp: 2 },
    { title: "three", timestamp: 3 }
  ],
  counter: 3,
  singlePost: 0,
  user: ""
};

const initialUserBase = {
  userCollection: [{ userName: "", password: "" }]
};

const counter = (state = 0, action) => {
  switch (action.type) {
    case "INCREMENT":
      return state + 1;
    case "DECREMENT":
      return state - 1;
    default:
      return state;
  }
};

const posts = (state = initialState, action) => {
  switch (action.type) {
    case "ADD":
      return {
        postsCollection: [...state.postsCollection, action.post],
        counter: state.counter
      };
    case "DELETE":
      return {
        postsCollection: state.postsCollection.filter(
          p => p.timestamp !== action.timestamp
        ),
        counter: state.counter
      };

    /*case "FETCH_POSTS":
      return {
        ...state,
        postsCollection: action.payload
      };*/

    case "SHOW_POST":
      return { ...state, singlePost: action.postToShowId };

    default:
      return state;
  }
};

const session = (state = initialState, action) => {
  switch (action.type) {
    case "LOG":
      console.log(action.user);
      return {
        user: action.user,
        token: action.token
      };

    case "REGISTER":
      console.log(action.user);
      return {
        user: action.user,
        password: action.password,
        token: action.token
      };

    default:
      return state;
  }
};

const rootReducer = combineReducers({
  posts: posts,
  counter: counter,
  session: session
});

const store = createStore(rootReducer);

export default store;
