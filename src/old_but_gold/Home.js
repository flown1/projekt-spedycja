import React from "react";
import { connect } from "react-redux";

class Home extends React.Component {
  increment = () => this.props.dispatch({ type: "INCREMENT" });
  decrement = () => this.props.dispatch({ type: "DECREMENT" });
  render() {
    return (
      <div>
        <p style={{ color: "white", fontSize: "2.5vw" }}>
          {" "}counter: {this.props.counter}
        </p>
        <button className="btn btn-danger" onClick={this.increment}>
          increment
        </button>
        <button className="btn btn-danger" onClick={this.decrement}>
          decrement
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    counter: state.counter
  };
};
export default connect(mapStateToProps)(Home);
