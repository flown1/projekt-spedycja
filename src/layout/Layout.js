'use-strict';

import React from "react";
import { Link } from "react-router";
import styled from "styled-components";
import { connect } from "react-redux";
import * as LoginAction from '../store/LoginActions';
import CheckLayout from "./CheckLayout";
import 'babel-polyfill';
import { ReactSlackChat } from 'react-slack-chat';

class Layout extends React.Component {
  onChange(e){
    e.preventDefault()
    this.props.onLogout();
    this.props.onChangeStatus();
  }
  render() {
    if((this.props.showLogin===true)&&(this.props.showLoader===true)){
    return (
      <div>
        <StyledComp>
          <StyledUl>
            <li>
              <StyledLink to="/assignments">Baza zleceń</StyledLink>
            </li>
            <li>
              <StyledLink to="/myorders">Moje zlecenia</StyledLink>
            </li>
            <li>
              <StyledLink to="/driver">Profil</StyledLink>
            </li>
            <li>
              <StyledLink to="/settings">Ustawienia
              </StyledLink>
            </li>
            <li>
              <StyledLink to="/login" onClick={this.onChange.bind(this)}>Wyloguj</StyledLink>
            </li>
          </StyledUl>
        </StyledComp>
        <StyledContain>
          <div style={{zIndex: "100"}}>
        <ReactSlackChat
          botName={this.props.user.name + " " + this.props.user.surname} //
          apiToken='eG94Yi0yODc0NzY1OTQ1MTktQWdYT0JURjRDa3FiNzQ2U3pjb25rRVRi'
          channels={[
          {
            name: 'Ogólny',
            id:   'C8EGXFMKP'
          },
          {
            name: 'Kontakt z zarządzającym',
            id: 'C8DT00TFB'
          }]}
          helpText='Czat'
          themeColor='#0294A5'
          userImage='http://www.iconshock.com/img_vista/FLAT/mail/jpg/robot_icon.jpg'
          debugMode={true}
          hooks={[
            {
              /* My Custom Hook */
              id: 'getSystemInfo',
              action: () => 'MY SYSTEM INFO!'
            }
          ]}
        /></div>
       <CheckLayout buff={this.props.children}/>
        </StyledContain>
      </div>
    );}
    else{return(<div>
      <StyledComp>
        <StyledUl>
          <li>
            <StyledLink to="/login">Zaloguj!</StyledLink>
          </li>
        </StyledUl>
      </StyledComp>
      <StyledContain>
      <CheckLayout buff={this.props.children}/>      
      </StyledContain>
    </div>);}
  }
}

const mapStateToProps = (state) => {
  return { showLogin: state.showLogin,
          showLoader: state.showLoader,
          user: state.user};
};
const mapDispatchToProps = (dispatch) => {
  return {onChangeStatus: () => dispatch(LoginAction.setFalse()),
  onLogout: () => dispatch(LoginAction.logout())};
};

const StyledLink = styled(Link)`
  margin-left: 2vw;
  margin-right: 2vw;
  color: white;
  text-decoration: none;
    &:hover {
      color:  #03353E !important ;
      text-decoration: none;
    }
    &:active {
      color: #03353E ;
      text-decoration: none;
    }
    &:visited {
      color: white;
      text-decoration: none;
    }

    &:link {
      color: white; ;
      text-decoration: none;
    }
`;


const StyledContain = styled.div`
  margin: 0 auto;
  width: 100%;
  height: 40vw;
  display: flex;
  justify-content: center;
  align-items:center;
`;

const StyledComp = styled.div`
  width: 100%;
  color: white;
  background-color: #0294A5;
  display: flex;
  justify-content: center;
  text-align: center;
  margin-bottom: 30px;
  border-bottom: 2px solid;
`;

const StyledUl = styled.ul`
font-size: 2vw;
height: 60px;
display: flex;
align-items:center;
justify-content:center;
list-style-type: none;
margin: 0;
padding: 0;
width:100%;
`;

export default connect(mapStateToProps,mapDispatchToProps)(Layout);
