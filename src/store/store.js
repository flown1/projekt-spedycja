import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

 const initialStateUser={
    user: 
        {name: "",
        surname: "",
        company: "",
        email: "",
        password: "",
        number: "",
        token: ""},
    assignment:{
        assignment_id: " ",
        start_location_name: " ",
        finish_location_name: " ",
        start_date: " ",
        finish_date: " ",
        status_id:" ",
        vehicle_id:" ",
        ware_name: " ",
        finish_location_latitude: " ",
        finish_location_longitude: " ",
        start_location_latitude: " ",
        start_location_longitude: " "
    },
    orders: [],
    myorders: [],
    showLogin: false,
    showLoader: false
}

const userReducer = (state=initialStateUser,action) => {
    switch(action.type) {
        case 'CHANGE_EMAIL':{
            return {...state, user : {...state.user, email: action.email}}}
        case 'CHANGE_PASSWORD':{
            return {...state, user : {...state.user, password: action.password}}}
        case 'CHANGE_USERTOKEN':
            return {...state, user : {...state.user, token: action.token}}
        case 'CHANGE_NAME':
            return {...state, user : {...state.user, name: action.name}}
        case 'CHANGE_SURNAME':
            return {...state, user : {...state.user, surname: action.surname}}
        case 'CHANGE_COMPANY':
            return {...state, user : {...state.user, company: action.company}}
        case 'CHANGE_NUMBER':
            return {...state, user : {...state.user, number: action.number}}
        case 'CHANGE_ASSIGNMENT_ID':
            return {...state, assignment : {...state.assignment, assignment_id: action.assignmet_id}}
        case 'CHANGE_START_LOCATION':
            return {...state, assignment : {...state.assignment, start_location_name: action.start_location_name}}
        case 'CHANGE_FINISH_LOCATION':
            return {...state, assignment : {...state.assignment, finish_location_name: action.finish_location_name}}
        case 'CHANGE_START_DATE':
            return {...state, assignment : {...state.assignment, start_date: action.start_date}}
        case 'CHANGE_FINISH_DATE':
            return {...state, assignment : {...state.assignment, finish_date: action.finish_date}}
        case 'CHANGE_STATUS_ID':
            return {...state, assignment : {...state.assignment, status_id: action.status_id}}
        case 'CHANGE_VEHICLE_ID':
            return {...state, assignment : {...state.assignment, vehicle_id: action.vehicle_id}}
        case 'CHANGE_WARE_NAME':
            return {...state, assignment : {...state.assignment, ware_name: action.ware_name}}
        case 'CHANGE_LOCATIONS':
            return {...state, assignment : {...state.assignment,  finish_location_latitude: action.flat,
            finish_location_longitude: action.flong,
            start_location_latitude: action.slat,
            start_location_longitude:  action.slong}}
        case 'CHANGE_ORDERS':
            return {...state, orders: action.value}
        case 'CHANGE_STATUS_TRUE':     
            return {...state,showLogin: true}
        case 'CHANGE_STATUS_FALSE':
            return {...state,showLogin: false}
        case 'CHANGE_LOADER_TRUE':     
            return {...state,showLoader: true}
        case 'CHANGE_LOADER_FALSE':
            return {...state,showLoader: false}
        case 'CHANGE_MY_ORDERS':
            return {...state, myorders: action.value}
        case 'LOGOUT':
            return {state: undefined}
        default:
        return state;
    }
}

const store = createStore(userReducer, compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

export default store;