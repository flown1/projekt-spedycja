export const CHANGE_STATUS_TRUE = 'CHANGE_STATUS_TRUE';
export const CHANGE_STATUS_FALSE = 'CHANGE_STATUS_FALSE';
export const CHANGE_STATUS_EMAIL = 'CHANGE_EMAIL';
export const CHANGE_STATUS_USERTOKEN = 'CHANGE_USERTOKEN';
export const CHANGE_STATUS_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_STATUS_NAME = 'CHANGE_NAME';
export const CHANGE_STATUS_SURNAME = 'CHANGE_SURNAME';
export const CHANGE_STATUS_COMPANY= 'CHANGE_COMPANY';
export const CHANGE_STATUS_NUMBER= 'CHANGE_NUMBER';

export const CHANGE_LOCATIONS= 'CHANGE_LOCATIONS';

export const CHANGE_STATUS_ASSIGNMENT_ID= 'CHANGE_ASSIGNMENT_ID';
export const CHANGE_STATUS_START_LOCATION = 'CHANGE_START_LOCATION';
export const CHANGE_STATUS_FINISH_LOCATION= 'CHANGE_FINISH_LOCATION';
export const CHANGE_STATUS_START_DATE= 'CHANGE_START_DATE';
export const CHANGE_STATUS_FINISH_DATE= 'CHANGE_FINISH_DATE';
export const CHANGE_STATUS_STATUS_ID= 'CHANGE_STATUS_ID';
export const CHANGE_STATUS_VEHICLE_ID= 'CHANGE_VEHICLE_ID';
export const CHANGE_STATUS_WARE_NAME= 'CHANGE_WARE_NAME';
export const CHANGE_STATUS_ORDERS= 'CHANGE_ORDERS';

export const CHANGE_STATUS_MY_ORDERS= 'CHANGE_MY_ORDERS';

export function setTrue() {
    return{
        type: CHANGE_STATUS_TRUE
    };
}

export function setFalse() {
    return{
        type: CHANGE_STATUS_FALSE
    };
}
export function setLoaderTrue() {
    return{
        type: 'CHANGE_LOADER_TRUE'
    };
}

export function setLoaderFalse() {
    return{
        type: 'CHANGE_LOADER_FALSE'
    };
}


export function setEmail(value) {
    return{
        type: CHANGE_STATUS_EMAIL,
        email: value
    };
}


export function setPassword(value) {
    return{
        type: CHANGE_STATUS_PASSWORD,
        password: value
    };
}


export function setName(value) {
    return{
        type: CHANGE_STATUS_NAME,
       name: value
    };
}


export function setSurname(value) {
    return{
        type: CHANGE_STATUS_SURNAME,
        surname: value
        
    };
}

export function setUserToken(value) {
    return{
        type: CHANGE_STATUS_USERTOKEN,
        token: value
        
    };
}



export function setCompany(value) {
    return{
        type: CHANGE_STATUS_COMPANY,
        company: value        
    };
}

export function setNumber(value) {
    return{
        type: CHANGE_STATUS_NUMBER,
        number: value        
    };
}

/*----*/
export function setAssignmentId(value) {
    return{
        type: CHANGE_STATUS_ASSIGNMENT_ID,
        assignmet_id: value        
    };
}


export function setStartLocation(value) {
    return{
        type: CHANGE_STATUS_START_LOCATION,
        start_location_name: value        
    };
}


export function setFinishLocation(value) {
    return{
        type: CHANGE_STATUS_FINISH_LOCATION,
        finish_location_name: value        
    };
}


export function setStartDate(value) {
    return{
        type: CHANGE_STATUS_START_DATE,
        start_date: value        
    };
}


export function setFinishDate(value) {
    return{
        type: CHANGE_STATUS_FINISH_DATE,
        finish_date: value        
    };
}


export function setStatusID(value) {
    return{
        type: CHANGE_STATUS_STATUS_ID,
        status_id: value        
    };
}


export function setVehicleID(value) {
    return{
        type: CHANGE_STATUS_VEHICLE_ID,
        vehicle_id: value        
    };
}


export function setWareName(value) {
    return{
        type: CHANGE_STATUS_WARE_NAME,
        ware_name: value        
    };
}

export function setTude(str_long, str_lat, fsh_long, fsh_lat){
    return{
        type: CHANGE_LOCATIONS,
        slong: str_long,
        slat: str_lat,
        flong: fsh_long,
        flat: fsh_lat
    }
}

export function setOrders(value) {
    return{
        type: CHANGE_STATUS_ORDERS,
       value      
    };
}

export function setMyOrders(value) {
    return{
        type: CHANGE_STATUS_MY_ORDERS,
       value      
    };
}

export function logout(){
    return{
        type: 'LOGOUT'
    };
}

