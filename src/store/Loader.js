import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as LoginAction from './LoginActions';
import { hashHistory } from "react-router";
import axios from 'axios';

class Loader extends Component{
  constructor(props){
    super(props);
    this.state = {
      val: false}}

/****Login****/
    onSubmit(){
    const ROOT_URL = "https://spedition-app.herokuapp.com";
    let self = this;
    axios.post(ROOT_URL + '/auth/login',{
      email: self.props.user.email,
      password: self.props.user.password
    })
      .then(function (response) {
        if(response.status === 200){
          self.props.onChangeStatus();
          self.props.onChangeUserToken(response.data.auth_token);
          self.props.onChangePassword(self.props.user.password);
          self.props.onChangeEmail(self.props.user.email);
          self.fetchDriver(response.data.auth_token);
          self.fetchAssigments(response.data.auth_token);
        }else{
          console.log('response error ');
        }
      })
      .catch(function (error) {
        alert("Invalid login or password! Try again!");
      });
  }

/****Driver****/
  fetchDriver(token){
    const ROOT_URL = 'https://spedition-app.herokuapp.com';
    let self = this;
    axios.get(ROOT_URL + '/profile',{
      headers:{
        'Authorization':  token
      }
    })
      .then(function (response) {
        console.log("wchodzi kierowca");
        let data = response.data;
        self.props.onChangeName(data.name);
        self.props.onChangeSurname(data.surname);
        self.props.onChangeNumber(data.phone_number);
    })
    .catch(function (error) {
      console.log("Used token: ", token);
      console.log("Nie mozna pobrac zlecenia!");
    });
  }

  /****Zlecenie****/
  fetchAssigments(token){
    const ROOT_URL = 'https://spedition-app.herokuapp.com';
    let self = this;
    axios.get(ROOT_URL + '/orders/my',{
      headers:{
        'Authorization':  token
      }
    })
      .then(function (response) {
        console.log("wchodzi zlecenie");
        let data = response.data.filter((item) => {
          return item.status.id === 3;
        });
        if(data.length===0){
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var yyyy = today.getFullYear();
            if(dd<10) {
                dd = '0'+dd
            } 

            if(mm<10) {
                mm = '0'+mm
            } 
          today = mm +'/'+ dd +'/'+ yyyy;
          self.props.onChangeAssignmentID(0);
          self.props.onChangeStartLocationName("---");
          self.props.onChangeFinishLocationName("---");
          self.props.onChangeStartDate(today);
          self.props.onChangeFinishDate(today);
          self.props.onChangeStatusID(0);
          self.props.onChangeVehicleID(0);
          self.props.onChangeWareName(0);
          self.props.onChangeLocation(
            " ",
            " ",
            " ",
            " ");
          self.props.onChangeMyOrders([]);
          self.setState({val: true})
        }else{
        console.log(data);
        self.props.onChangeAssignmentID(data[0].id);
        self.props.onChangeStartLocationName(data[0].start_location.city);
        self.props.onChangeFinishLocationName(data[0].finish_location.city);
        self.props.onChangeStartDate(data[0].start_date);
        self.props.onChangeFinishDate(data[0].finish_date);
        self.props.onChangeStatusID(data[0].status.id);
        self.props.onChangeVehicleID(data[0].vehicle_id);
        self.props.onChangeWareName(data[0].ware.type_name);
        self.props.onChangeLocation(
          data[0].start_location.longitude,
          data[0].start_location.latitude,
          data[0].finish_location.longitude,
          data[0].finish_location.latitude);
        self.props.onChangeMyOrders(data);
        self.setState({val: true})}
    })
    .catch(function (error) {
      console.log("Used token: ", token);
      console.log("Nie mozna pobrac zlecenia!");
    });

  }

  /****Zlecenia****/
    fetchData(){
    /*
      Ze zlecen jest zbierane tylko ID i Status bo więcej info
       tak na prawdę jeszcze nie ma
    */
    const ROOT_URL = "http://spedition-app.herokuapp.com";
    let self = this;
    axios.get(ROOT_URL + '/orders')
      .then(function (response) {
        console.log("wchodza zlecenia");
        console.log(response);
        self.props.onChangeOrders(response.data);
        self.props.onChangeLoader();
      })
      .catch(function (error) {
        alert("Błąd. Nie udało się wyświetlić zleceń")
        console.log(error);
      });
  }

  componentWillMount(){
      this.onSubmit();
      this.fetchData();
    }

  render(){
    var val = false;
    while(val===false){
    if(this.state.val===true){
      val = true;
    return(
        <div>
          {hashHistory.push('/driver')}
        </div>
    )}
    else {
      return(<div></div>)
    }
  }}
}
const mapStateToProps = (state) => {
  return { 
    showLogin: state.showLogin,
    userToken: state.user.token,
    assignment: state.assignment,
    orders: state.orders, 
    user: state.user};
};
const mapDispatchToProps = (dispatch) => {
  return {
      /***Logowanie***/
    onChangeStatus: () => dispatch(LoginAction.setTrue()),
    onChangeLoader: () => dispatch(LoginAction.setLoaderTrue()),
    onChangeEmail: (value) => dispatch(LoginAction.setEmail(value)),
    onChangePassword: (value) => dispatch(LoginAction.setPassword(value)),
    onChangeUserToken: (value) => dispatch(LoginAction.setUserToken(value)),
    /***Zlecenie***/
    onChangeAssignmentID: (value) => dispatch(LoginAction.setAssignmentId(value)),
    onChangeStartLocationName: (value) => dispatch(LoginAction.setStartLocation(value)),
    onChangeFinishLocationName: (value) => dispatch(LoginAction.setFinishLocation(value)),
    onChangeStartDate: (value) => dispatch(LoginAction.setStartDate(value)),
    onChangeFinishDate: (value) => dispatch(LoginAction.setFinishDate(value)),
    onChangeStatusID: (value) => dispatch(LoginAction.setStatusID(value)),
    onChangeVehicleID: (value) => dispatch(LoginAction.setVehicleID(value)),
    onChangeWareName: (value) => dispatch(LoginAction.setWareName(value)),
    onChangeLocation: (val1,val2,val3,val4) => dispatch(LoginAction.setTude(val1,val2,val3,val4)),
    /***Zlecenia***/
    onChangeOrders: (value) => dispatch(LoginAction.setOrders(value)),
    onChangeMyOrders: (value) => dispatch(LoginAction.setMyOrders(value)),
    /***Profil***/
    onChangeName: (value) => dispatch(LoginAction.setName(value)),
    onChangeSurname: (value) => dispatch(LoginAction.setSurname(value)),
    onChangeNumber: (value) => dispatch(LoginAction.setNumber(value))};
};
export default connect(mapStateToProps,mapDispatchToProps)(Loader);