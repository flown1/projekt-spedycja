import React, { Component } from "react";
import "./App.css";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import styled from "styled-components";
import Layout from "./layout/Layout";
import LoginForm from "./components/resources/LoginForm";
import DriveOverview from "./components/pages/DriverOverview";
import Setting from "./components/pages/Setting";
import AssigmentsPage from "./components/pages/AssigmentsPage";
import Loader from "./store/Loader"
import MyAssignments from "./components/pages/MyAssignments";

class App extends Component {
  render() {
    return (
      <Container>
        <Router history={hashHistory}>
          <Route path="/" component={Layout}>
            <IndexRoute component={LoginForm} />
            <Route path="loader" component={Loader} />
            <Route path="login" component={LoginForm} />
            <Route path="myorders" component={MyAssignments} />
            <Route path="driver" component={DriveOverview} />
            <Route path="settings" component={Setting} />
            <Route path="assignments" component={AssigmentsPage} />
          </Route>
        </Router>
      </Container>);
  }
}

const Container = styled.div`
width:100vw;
margin: 0 auto;
font-family: 'Racing Sans One', cursive;
`;

export default App;
